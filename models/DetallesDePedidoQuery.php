<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DetallesDePedido]].
 *
 * @see DetallesDePedido
 */
class DetallesDePedidoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DetallesDePedido[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DetallesDePedido|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
