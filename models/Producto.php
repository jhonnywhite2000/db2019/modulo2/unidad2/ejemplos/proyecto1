<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $IdProducto
 * @property string $NombreProducto
 * @property int $IdProveedor
 * @property int $IdCategoria
 * @property string $CantidadPorUnidad
 * @property double $PrecioUnidad
 * @property int $UnidadesEnExistencia
 * @property int $UnidadesEnPedido
 * @property int $NivelNuevoPedido
 * @property string $Suspendido
 *
 * @property DetallesDePedido[] $detallesDePedidos
 * @property Pedido[] $pedidos
 * @property Categoria $categoria
 * @property Proveedor $proveedor
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdProveedor', 'IdCategoria', 'UnidadesEnExistencia', 'UnidadesEnPedido', 'NivelNuevoPedido'], 'integer'],
            [['PrecioUnidad'], 'number'],
            [['NombreProducto'], 'string', 'max' => 80],
            [['CantidadPorUnidad'], 'string', 'max' => 40],
            [['Suspendido'], 'string', 'max' => 1],
            [['IdCategoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['IdCategoria' => 'IdCategoria']],
            [['IdProveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedor::className(), 'targetAttribute' => ['IdProveedor' => 'IdProveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Id Producto',
            'NombreProducto' => 'Nombre Producto',
            'IdProveedor' => 'Id Proveedor',
            'IdCategoria' => 'Id Categoria',
            'CantidadPorUnidad' => 'Cantidad Por Unidad',
            'PrecioUnidad' => 'Precio Unidad',
            'UnidadesEnExistencia' => 'Unidades En Existencia',
            'UnidadesEnPedido' => 'Unidades En Pedido',
            'NivelNuevoPedido' => 'Nivel Nuevo Pedido',
            'Suspendido' => 'Suspendido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetallesDePedidos()
    {
        return $this->hasMany(DetallesDePedido::className(), ['IdProducto' => 'IdProducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['IdPedido' => 'IdPedido'])->viaTable('detalles_de_pedido', ['IdProducto' => 'IdProducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['IdCategoria' => 'IdCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['IdProveedor' => 'IdProveedor']);
    }
}
